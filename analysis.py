import pandas as pd
import re
import matplotlib.pyplot as plt
from datetime import datetime

number_regex = re.compile('\d+')

data = pd.read_csv('rpc_to_pcu.csv')
data.sort_values(by='date', ascending=True, inplace=True)

dates = list()
numbers = list()

previous_number = 0
for _, row in data.iterrows():
    number = int(re.findall(number_regex, row['tweet'])[0])
    if number != previous_number and number > 1:
        dates.append(datetime.fromisoformat(row['date']))
        numbers.append(number)
    previous_number = number


plt.plot(dates, numbers)
plt.gcf().autofmt_xdate()
plt.xlabel('Дата')
plt.ylabel('Кількість парафій')
plt.title('Переходи парафіій з УПЦ МП в ПЦУ')
plt.savefig('from_rpc_to_pcu.png', dpi=300)


