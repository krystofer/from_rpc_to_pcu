'''
Install twint: pip3 install git+https://github.com/twintproject/twint.git@origin/master#egg=twint

'''

import twint
import pandas as pd
import datetime

tweets_df_list = list()

for year in range(2019, 2023):
    for month in range(1, 12):
        print(f'{year} {month}')
        c = twint.Config()
        c.Username = "perehody_do_PCU"
        c.Search = ['УПЦ МП з ']
        c.Limit = 200
        c.Since = str(datetime.datetime(year, month, 1))
        c.Until = str(datetime.datetime(year, month + 1, 1))

        c.Pandas = True

        twint.run.Search(c)

        tweets_df = twint.storage.panda.Tweets_df
        tweets_df_list.append(tweets_df)

all_tweets = pd.concat(tweets_df_list)

all_tweets.to_csv('rpc_to_pcu.csv', index=False)